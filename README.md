# Customer API Gateway

## Резюме

Название: Customer API Gateway  
Домен: Customer GUI  
Назначение: Backend-for-Frontend (BFF) для клиентского фронта приложения  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Gitlab](https://ensi-platform.gitlab.io/docs/tech/back)

Регламент работы над задачами тоже находится в [Gitlab](https://ensi-platform.gitlab.io/docs/guid/regulations)

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Зависимости

|Название|Описание|Переменные окружения|
|---|---|---|
| Orders | Ensi Baskets | ORDERS_BASKETS_SERVICE_HOST |

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/customer-gui/job/customer-api-gateway/  
URL: https://customer-api-gateway-master-dev.ensi.tech/docs/swagger

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).

