<?php

namespace App\Domain\Catalog\Tests\Factories\Classifiers;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\EnsiFileFactory;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\BrandResponse;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\SearchBrandsResponse;

class BrandFactory extends BaseApiFactory
{
    public ?string $logoUrl = null;
    public ?string $fileUrl = null;

    protected function definition(): array
    {
        return [
            'id' => $this->requiredId(),

            'is_active' => $this->faker->boolean,
            'name' => $this->faker->text(20),
            'code' => $this->faker->word(),
            'description' => $this->faker->text(50),

            'logo_file' => $this->when($this->logoUrl === null, fn () => $this->generateFile()),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'logo_url' => $this->logoUrl,
        ];
    }

    public function make(array $extra = []): Brand
    {
        return new Brand($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): BrandResponse
    {
        return new BrandResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchBrandsResponse
    {
        return $this->generateResponseSearch(SearchBrandsResponse::class, $extra, $count);
    }

    public function withExternalLogo(?string $value): self
    {
        $value ??= $this->faker->imageUrl;

        return $this->immutableSet('logoUrl', $value);
    }

    public function withFileUrl(string $value): self
    {
        return $this->immutableSet('fileUrl', $value);
    }

    private function generateFile(): File
    {
        $extra = $this->fileUrl !== null ? ['url' => $this->fileUrl] : [];

        return new File(EnsiFileFactory::new()->make($extra));
    }
}
