<?php

namespace App\Domain\Catalog\Tests\Factories\Offers;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferResponse;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Ensi\OffersClient\Dto\Stock;

class OfferFactory extends BaseApiFactory
{
    protected array $stocks = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->requiredId(),
            'product_id' => $this->foreignId(),
            'seller_id' => $this->foreignId(),
            'external_id' => $this->faker->numerify('##-##'),
            'sale_status' => $this->faker->randomElement(OfferSaleStatusEnum::getAllowableEnumValues()),
            'storage_address' => $this->faker->optional()->address,
            'base_price' => $this->faker->numberBetween(0, 100_000),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->stocks) {
            $definition['stocks'] = $this->stocks;
        }

        return $definition;
    }

    public function withStock(?Stock $stock = null): self
    {
        $this->stocks[] = $stock ?: StockFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Offer
    {
        return new Offer($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchOffersResponse
    {
        return $this->generateResponseSearch(SearchOffersResponse::class, $extra, $count);
    }

    public function makeResponseOne(array $extra = []): OfferResponse
    {
        return new OfferResponse([
            'data' => $this->make($extra),
        ]);
    }
}
