<?php

namespace App\Domain\Catalog\Tests\Factories\Offers;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\OffersClient\Dto\SearchStocksResponse;
use Ensi\OffersClient\Dto\Stock;

class StockFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->requiredId(),
            'store_id' => $this->foreignId(),
            'offer_id' => $this->foreignId(),
            'product_id' => $this->foreignId(),
            'qty' => $this->faker->randomFloat(4),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Stock
    {
        return new Stock($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchStocksResponse
    {
        return $this->generateResponseSearch(SearchStocksResponse::class, $extra, $count);
    }
}
