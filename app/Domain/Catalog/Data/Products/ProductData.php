<?php

namespace App\Domain\Catalog\Data\Products;

use Ensi\OffersClient\Dto\Offer;
use Ensi\PimClient\Dto\Product;
use Illuminate\Support\Traits\ForwardsCalls;

/**
 * @mixin Product
 */
class ProductData
{
    use ForwardsCalls;

    public function __construct(
        public readonly Product $product,
        public readonly ?Offer $offer = null
    ) {
    }

    public function __call(string $name, array $arguments)
    {
        return $this->forwardDecoratedCallTo($this->product, $name, $arguments);
    }

    public function getBasePrice(): ?int
    {
        return $this->offer?->getBasePrice();
    }
}
