<?php

namespace App\Domain\Orders\Data\Baskets;

use Ensi\BasketsClient\Dto\BasketItem;
use Ensi\OffersClient\Dto\Offer;
use Ensi\PimClient\Dto\Product;
use Illuminate\Support\Traits\ForwardsCalls;

/**
 * @mixin Product
 */
class BasketItemData
{
    use ForwardsCalls;

    public function __construct(
        public readonly Product $product,
        public readonly Offer $offer,
        public readonly BasketItem $basketItem,
    ) {
    }

    public function __call(string $name, array $arguments)
    {
        return $this->forwardDecoratedCallTo($this->product, $name, $arguments);
    }

    public function getOfferId(): int
    {
        return $this->basketItem->getOfferId();
    }

    public function getQty(): float
    {
        return $this->basketItem->getQty();
    }

    public function getBasePrice(): ?int
    {
        return $this->offer->getBasePrice();
    }
}
