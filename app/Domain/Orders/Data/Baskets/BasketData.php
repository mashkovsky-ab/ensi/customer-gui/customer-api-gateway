<?php

namespace App\Domain\Orders\Data\Baskets;

use Ensi\BasketsClient\Dto\Basket;
use Illuminate\Support\Traits\ForwardsCalls;

/**
 * @mixin Basket
 */
class BasketData
{
    use ForwardsCalls;

    /**
     * @var BasketItemData[]
     */
    protected array $items = [];

    public function __construct(
        public readonly Basket $basket,
    ) {
    }

    public function __call(string $name, array $arguments)
    {
        return $this->forwardDecoratedCallTo($this->basket, $name, $arguments);
    }

    /**
     * @param BasketItemData $item
     */
    public function addBasketItem(BasketItemData $item)
    {
        $this->items[] = $item;
    }

    /**
     * @return BasketItemData[]
     */
    public function getBasketItems(): array
    {
        return $this->items;
    }
}
