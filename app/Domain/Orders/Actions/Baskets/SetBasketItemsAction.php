<?php

namespace App\Domain\Orders\Actions\Baskets;

use Ensi\BasketsClient\Api\CustomerBasketsApi as BasketApi;
use Ensi\BasketsClient\Dto\SetBasketItemRequest;

class SetBasketItemsAction
{
    public function __construct(
        protected BasketApi $basketApi,
    ) {
    }

    public function execute(array $data): void
    {
        $request = new SetBasketItemRequest([
            'customer_id' => user()?->getCustomerId(),
            'items' => $data['items'],
        ]);

        $this->basketApi->setBasketCustomerItem($request);
    }
}
