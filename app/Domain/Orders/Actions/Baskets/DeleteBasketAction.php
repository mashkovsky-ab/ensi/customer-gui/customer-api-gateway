<?php

namespace App\Domain\Orders\Actions\Baskets;

use Ensi\BasketsClient\Api\CustomerBasketsApi as BasketApi;
use Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest;

class DeleteBasketAction
{
    public function __construct(
        protected BasketApi $basketApi,
    ) {
    }

    public function execute(): void
    {
        $request = new DeleteBasketCustomerRequest(['customer_id' => user()?->getCustomerId()]);
        $this->basketApi->deleteBasketCustomer($request);
    }
}
