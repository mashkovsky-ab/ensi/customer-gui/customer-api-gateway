<?php

namespace App\Domain\Orders\Tests\Baskets\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\BasketsClient\Dto\BasketItem;

class BasketItemFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->requiredId(),
            'basket_id' => $this->faker->numberBetween(1),
            'offer_id' => $this->faker->numberBetween(1),
            'seller_id' => $this->faker->numberBetween(1),
            'product_id' => $this->faker->numberBetween(1),
            'qty' => $this->faker->randomFloat(2, 1, 100),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): BasketItem
    {
        return new BasketItem($this->makeArray($extra));
    }
}
