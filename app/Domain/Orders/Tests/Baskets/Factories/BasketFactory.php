<?php

namespace App\Domain\Orders\Tests\Baskets\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\BasketsClient\Dto\Basket;
use Ensi\BasketsClient\Dto\BasketItem;
use Ensi\BasketsClient\Dto\SearchBasketCustomerResponse;

class BasketFactory extends BaseApiFactory
{
    protected array $items = [];

    protected function definition(): array
    {
        return [
            'id' => $this->requiredId(),
            'customer_id' => $this->faker->numberBetween(1),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'items' => $this->items,
        ];
    }

    public function withItems(?BasketItem $item = null): self
    {
        $this->items[] = $item ?: BasketItemFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Basket
    {
        return new Basket($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = []): SearchBasketCustomerResponse
    {
        return new SearchBasketCustomerResponse([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }
}
