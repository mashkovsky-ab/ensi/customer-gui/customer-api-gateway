<?php


namespace App\Domain\Common\Actions;

use App\Domain\Common\ProtectedFiles\ProtectedFileFactory;
use App\Domain\Common\ProtectedFiles\ProtectedFileLoaderFactory;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Storage;

class GetProtectedFilePathAction
{
    public function __construct(
        protected EnsiFilesystemManager $filesystemManager,
        protected ProtectedFileFactory $fileFactory,
        protected ProtectedFileLoaderFactory $loaderFactory,
    ) {
    }

    public function execute(array $data): ?string
    {
        $file = $this->fileFactory->build($data['entity']);
        $file->fillFromArray($data);

        $loader = $this->loaderFactory->build($file);
        $rootPath = $loader->getRootPath();

        return $rootPath ? Storage::disk($this->filesystemManager->rootDiskName())->path($rootPath) : null;
    }
}
