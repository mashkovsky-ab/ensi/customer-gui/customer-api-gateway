<?php


namespace App\Domain\Common\ProtectedFiles;

abstract class ProtectedFileLoader
{
    protected ProtectedFile $file;

    public function setFile(ProtectedFile $file): static
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Получить путь до файла от корня диска ensi. Если файла, соответствующего параметрам не нашлось, то null
     * @return string|null
     */
    abstract public function getRootPath(): ?string;
}
