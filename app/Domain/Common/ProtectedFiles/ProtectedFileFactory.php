<?php

namespace App\Domain\Common\ProtectedFiles;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProtectedFileFactory
{
    public function build($entity): ProtectedFile
    {
        return match ($entity) {
            default => throw new ModelNotFoundException("Не удалось определить сущность"),
        };
    }
}
