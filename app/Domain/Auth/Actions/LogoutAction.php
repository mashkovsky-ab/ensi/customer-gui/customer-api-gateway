<?php

namespace App\Domain\Auth\Actions;

use Ensi\CustomerAuthClient\Api\OauthApi;
use Ensi\CustomerAuthClient\ApiException;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;

class LogoutAction
{
    public function __construct(private OauthApi $oauthApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(string $token): void
    {
        $tokenId = (new Parser(new JoseEncoder()))->parse($token)->claims()->all()['jti'];
        $this->oauthApi->getConfig()->setAccessToken($token);
        $this->oauthApi->deleteToken($tokenId);
    }
}
