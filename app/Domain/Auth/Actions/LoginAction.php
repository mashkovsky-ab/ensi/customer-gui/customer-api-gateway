<?php

namespace App\Domain\Auth\Actions;

use Ensi\CustomerAuthClient\Api\OauthApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\CreateTokenRequest;
use Ensi\CustomerAuthClient\Dto\CreateTokenResponse;
use Ensi\CustomerAuthClient\Dto\ErrorResponse;
use Ensi\CustomerAuthClient\Dto\GrantTypeEnum;

class LoginAction
{
    public function __construct(private OauthApi $oauthApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): CreateTokenResponse|ErrorResponse
    {
        $request = new CreateTokenRequest();
        $request->setGrantType(GrantTypeEnum::PASSWORD);
        $request->setClientId(
            config('openapi-clients.customers.customer-auth.client.id')
        );
        $request->setClientSecret(
            config('openapi-clients.customers.customer-auth.client.secret')
        );
        $request->setUsername($fields['login']);
        $request->setPassword($fields['password']);

        return $this->oauthApi->createToken($request);
    }
}
