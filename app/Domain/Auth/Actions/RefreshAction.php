<?php

namespace App\Domain\Auth\Actions;

use Ensi\CustomerAuthClient\Api\OauthApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\CreateTokenRequest;
use Ensi\CustomerAuthClient\Dto\CreateTokenResponse;
use Ensi\CustomerAuthClient\Dto\ErrorResponse;
use Ensi\CustomerAuthClient\Dto\GrantTypeEnum;

class RefreshAction
{
    public function __construct(private OauthApi $oauthApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): CreateTokenResponse|ErrorResponse
    {
        $request = new CreateTokenRequest();
        $request->setGrantType(GrantTypeEnum::REFRESH_TOKEN);
        $request->setClientId(
            config('openapi-clients.customers.customer-auth.client.id')
        );
        $request->setClientSecret(
            config('openapi-clients.customers.customer-auth.client.secret')
        );
        $request->setRefreshToken($fields['refresh_token']);

        return $this->oauthApi->createToken($request);
    }
}
