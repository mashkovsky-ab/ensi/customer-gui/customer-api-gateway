<?php

namespace App\Domain\Auth;

use App\Domain\Auth\Models\User;
use Illuminate\Contracts\Session\Session;

class SessionUserStorage
{
    /**
     * Ключ для хранения данных пользователя в сессии.
     */
    public const SESSION_KEY = 'user_details';

    /**
     * Ключ в данных пользователя для токена авторизации.
     */
    public const FIELDS_KEY = 'fields';

    public function __construct(private Session $session)
    {
    }

    public function load(): ?User
    {
        $attrs = $this->getUserData();
        if (empty($attrs) || !isset($attrs[self::FIELDS_KEY])) {
            return null;
        }

        $user = $this->constructUser($attrs[self::FIELDS_KEY]);
        if (isset($attrs[$user->getRememberTokenName()])) {
            $user->setRememberToken($attrs[$user->getRememberTokenName()]);
        }

        return $user;
    }

    public function save(User $user): void
    {
        $data = [
            self::FIELDS_KEY => $user->toArray(),
            $user->getRememberTokenName() => $user->getRememberToken(),
        ];
        $this->putUserData($data);
    }

    protected function getUserData(): ?array
    {
        return $this->session->get(self::SESSION_KEY);
    }

    protected function putUserData(array $data): void
    {
        $this->session->put(self::SESSION_KEY, $data);
    }

    /**
     * Создает и возвращает новый экземпляр пользователя.
     *
     * @param array $source
     * @return User
     */
    protected function constructUser(array $source): User
    {
        return new User($source);
    }
}
