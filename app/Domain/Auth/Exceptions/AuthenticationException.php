<?php

declare(strict_types=1);

namespace App\Domain\Auth\Exceptions;

use RuntimeException;

class AuthenticationException extends RuntimeException
{
}
