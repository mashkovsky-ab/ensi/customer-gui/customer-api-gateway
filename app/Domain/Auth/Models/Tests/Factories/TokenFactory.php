<?php

namespace App\Domain\Auth\Models\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use Ensi\CustomerAuthClient\Dto\CreateTokenResponse;
use Lcobucci\JWT\Configuration;

class TokenFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $configuration = Configuration::forUnsecuredSigner();

        return [
            'access_token' => $configuration->builder()
                ->identifiedBy($this->faker->md5())
                ->getToken($configuration->signer(), $configuration->signingKey())
                ->toString(),
            'refresh_token' => $configuration->builder()
                ->identifiedBy($this->faker->md5())
                ->getToken($configuration->signer(), $configuration->signingKey())
                ->toString(),
            'expires_in' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): CreateTokenResponse
    {
        return new CreateTokenResponse($this->makeArray($extra));
    }
}
