<?php

namespace App\Domain\Auth\Models\Tests\Factories;

use App\Domain\Auth\Models\User;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class UserFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->requiredId(),
            'login' => $this->faker->userName(),
            'full_name' => $this->faker->name(),
            'short_name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->lastName(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->numerify('+7##########'),
            'active' => true,
            'roles' => [],
        ];
    }

    public function make(array $extra = []): User
    {
        return new User($this->makeArray($extra));
    }
}
