<?php

namespace App\Domain\Auth\Models;

use App\Domain\Auth\Models\Tests\Factories\UserFactory;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Support\Arrayable;

class User implements Authenticatable, Arrayable
{
    protected int $id = 0;

    protected int $customer_id = 0;

    protected string $login;

    protected array $roles = [];

    protected string $rememberToken;

    protected string $fullName;

    protected string $shortName;

    protected string $lastName;

    protected string $firstName;

    protected string $middleName;

    protected string $email;

    protected string $phone;

    protected int $active;

    /**
     * User constructor.
     * @param array|null $source
     */
    public function __construct(?array $source)
    {
        if ($source !== null) {
            $this->fill($source);
        }
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getCustomerId(): int
    {
        return $this->customer_id;
    }

    /**
     * Возвращает логин пользователя.
     *
     * @return string
     */
    public function getAuthIdentifierName(): string
    {
        return $this->login;
    }

    /**
     * Возвращает идентификатор пользователя.
     *
     * @return int
     */
    public function getAuthIdentifier(): int
    {
        return $this->id;
    }

    /**
     * Заглушка для пароля.
     *
     * @return string
     */
    public function getAuthPassword(): string
    {
        return '';
    }

    /**
     * Возвращает токен авторизации.
     *
     * @return string
     */
    public function getRememberToken(): string
    {
        return $this->rememberToken ?? '';
    }

    /**
     * Устанавливает токен авторизации.
     *
     * @param string $value
     */
    public function setRememberToken($value): void
    {
        $this->rememberToken = $value;
    }

    /**
     * Возвращает имя токена авторизации.
     *
     * @return string
     */
    public function getRememberTokenName(): string
    {
        return 'remember_token';
    }

    public function isActive(): bool
    {
        return (bool) $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive(int $active): void
    {
        $this->active = $active;
    }

    /**
     * Выгружает данные пользователя в массив.
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'full_name' => $this->fullName,
            'short_name' => $this->shortName,
            'last_name' => $this->lastName,
            'first_name' => $this->firstName,
            'middle_name' => $this->middleName,
            'email' => $this->email,
            'phone' => $this->phone,
            'active' => $this->active,
            'roles' => $this->roles,
        ];
    }

    /**
     * Заполняет данные пользователя из массива.
     * @param array $source
     */
    public function fill(array $source): void
    {
        $this->id = (int)($source['id'] ?? 0);
        $this->customer_id = (int)($source['customer_id'] ?? 0);
        $this->login = $source['login'] ?? '';
        $this->fullName = $source['full_name'] ?? '';
        $this->shortName = $source['short_name'] ?? '';
        $this->lastName = $source['last_name'] ?? '';
        $this->firstName = $source['first_name'] ?? '';
        $this->middleName = $source['middle_name'] ?? '';
        $this->email = $source['email'] ?? '';
        $this->phone = $source['phone'] ?? '';
        $this->active = (int)($source['active'] ?? 0);
        $this->roles = (array)($source['roles'] ?? []);
    }

    public static function factory(): UserFactory
    {
        return UserFactory::new();
    }
}
