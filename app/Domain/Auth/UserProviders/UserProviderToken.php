<?php

namespace App\Domain\Auth\UserProviders;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\RemoteUserProvider;
use Ensi\CustomerAuthClient\Api\OauthApi;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Log\LogManager;

class UserProviderToken extends UserProviderAbstract
{
    public function __construct(
        protected RemoteUserProvider $remoteProvider,
        protected OauthApi $oauthApi,
        protected Repository $configRepo,
        protected LogManager $logger
    ) {
    }

    /**
     * Устанавливает текущего пользователя по авторизационной паре.
     * @param  array  $credentials
     * @return User|Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials): User|Authenticatable|null
    {
        $token = $credentials[config('auth.storage_key') ?? 'api_token'];
        if (blank($token)) {
            return null;
        }

        // Принудительная авторизация, вместо токена используется строка {USER_ID}-{CUSTOMER_ID}
        [$userId, $customerId] = explode('-', $token);

        return new User([
            'id' => (int)$userId,
            'customer_id' => (int)$customerId,
        ]);

        // todo: Авторизация через токен
        /**
            try {
                return $this->remoteProvider->retrieveCurrent($token);
            } catch (Exception $exception) {
                $this->logger->error($exception->getMessage());

                return null;
            }
        */
    }

    /**
     * Проверяет, что авторизованный пользователь соответствует авторизационной паре.
     * @param Authenticatable|User $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable|User $user, array $credentials): bool
    {
        return true;
    }

    /**
     * Обновляет токен аутентификации.
     * @param Authenticatable|User $user
     * @param string $token
     */
    public function updateRememberToken(Authenticatable|User $user, $token)
    {
    }
}
