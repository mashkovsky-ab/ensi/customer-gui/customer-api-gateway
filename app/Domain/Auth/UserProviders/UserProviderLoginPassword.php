<?php

namespace App\Domain\Auth\UserProviders;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\RemoteUserProvider;
use App\Domain\Auth\SessionUserStorage;
use Ensi\CustomerAuthClient\Api\OauthApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\CreateTokenRequest;
use Ensi\CustomerAuthClient\Dto\GrantTypeEnum;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Log\LogManager;
use Illuminate\Support\Str;

class UserProviderLoginPassword extends UserProviderAbstract
{
    public function __construct(
        protected RemoteUserProvider $remoteProvider,
        protected OauthApi $oauthApi,
        protected Repository $configRepo,
        protected SessionUserStorage $storage,
        protected LogManager $logger
    ) {
        $this->user = $this->storage->load();
    }

    /**
     * Устанавливает текущего пользователя по авторизационной паре.
     * @param  array  $credentials
     * @return User|Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials): User|Authenticatable|null
    {
        $login = $this->parseLogin($credentials);
        if (blank($login) || !isset($credentials['password'])) {
            return null;
        }

        try {
            $token = $this->authorize($login, $credentials['password'] ?? '');
            $user = $this->remoteProvider->retrieveCurrent($token);
            $this->updateRememberToken($user, $token);

            return $user;
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());

            return null;
        }
    }

    /**
     * Проверяет, что авторизованный пользователь соответствует авторизационной паре.
     * @param Authenticatable|User $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable|User $user, array $credentials): bool
    {
        $login = $this->parseLogin($credentials);

        return !blank($login) && $login === $user->getAuthIdentifierName();
    }

    /**
     * Выбирает логин пользователя из переданного массива данных для аутентификации.
     * @param array $credentials
     * @return string|null
     */
    private function parseLogin(array $credentials): string|null
    {
        foreach ($credentials as $key => $value) {
            if (!Str::contains($key, 'password')) {
                return $value;
            }
        }

        return null;
    }

    /**
     * Выполняет авторизацию и возвращает полученный токен.
     * @param string $login
     * @param string $password
     * @return string
     * @throws ApiException
     */
    private function authorize(string $login, string $password): string
    {
        $request = new CreateTokenRequest([
            'grant_type' => GrantTypeEnum::PASSWORD,
            'client_id' => $this->configRepo->get(
                'openapi-clients.customers.customer-auth.client.id'
            ),
            'client_secret' => $this->configRepo->get(
                'openapi-clients.customers.customer-auth.client.secret'
            ),
            'scope' => [],
            'username' => $login,
            'password' => $password,
        ]);

        $response = $this->oauthApi->createToken($request);

        return $response->getAccessToken();
    }

    /**
     * Обновляет токен аутентификации.
     * @param  Authenticatable|User  $user
     * @param  string  $token
     */
    public function updateRememberToken(Authenticatable|User $user, $token)
    {
        $user->setRememberToken($token);
        $this->storage->save($user);
        $this->user = $user;
    }
}
