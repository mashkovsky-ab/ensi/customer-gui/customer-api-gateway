<?php

declare(strict_types=1);

namespace App\Providers;

use App\Domain\Auth\UserProviders\UserProviderLoginPassword;
use App\Domain\Auth\UserProviders\UserProviderToken;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     */
    protected $policies = [
      // 'App\Model' => 'App\Policies\ModelPolicy',
   ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();
    }

    public function register()
    {
        parent::register();

        $this->app->make('auth')->provider('ensi_login_password', function ($app) {
            return $app->make(UserProviderLoginPassword::class);
        });

        $this->app->make('auth')->provider('ensi_token', function ($app) {
            return $app->make(UserProviderToken::class);
        });
    }
}
