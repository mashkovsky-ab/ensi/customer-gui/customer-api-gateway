<?php

namespace App\Http\ApiV1\Support\Tests;

use App\Domain\Auth\Models\User;
use Ensi\BasketsClient\Api\CustomerBasketsApi as BasketApi;
use Ensi\CustomerAuthClient\Api\OauthApi;
use Ensi\CustomerAuthClient\Api\UsersApi as CustomerAuthUsersApi;
use Ensi\LaravelOpenApiTesting\ValidatesAgainstOpenApiSpec;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\PimClient\Api\ProductsApi;
use Mockery\MockInterface;
use Tests\ComponentTestCase;

abstract class ApiV1ComponentTestCase extends ComponentTestCase
{
    use ValidatesAgainstOpenApiSpec;

    protected function getOpenApiDocumentPath(): string
    {
        return public_path('api-docs/v1/index.yaml');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->authorize();
    }

    protected function authorize(User $user = null)
    {
        $this->actingAs($user ?? User::factory()->make());
        $this->withHeaders(['Authorization' => 'Bearer 123']);
    }

    // region basket
    public function mockBasketsBasketApi(): MockInterface|BasketApi
    {
        return $this->mock(BasketApi::class);
    }
    // endregion

    // region service oauth
    public function mockOauthApi(): MockInterface|OauthApi
    {
        return $this->mock(OauthApi::class);
    }

    // endregion

    // region service customer auth
    public function mockCustomerAuthUsersApi(): MockInterface|CustomerAuthUsersApi
    {
        return $this->mock(CustomerAuthUsersApi::class);
    }
    // endregion

    // region service pim
    public function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    // endregion

    // region service offers
    public function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }
    // endregion
}
