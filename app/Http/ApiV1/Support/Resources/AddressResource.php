<?php

namespace App\Http\ApiV1\Support\Resources;

use Ensi\LogisticClient\Dto\Address;
use Illuminate\Http\Request;

/**
 * Class AddressResource
 * @package App\Http\ApiV1\Support\Resources
 *
 * @mixin Address
 */
class AddressResource extends BaseJsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'address_string' => $this->getAddressString(),
            'post_index' => $this->getPostIndex(),
            'country_code' => $this->getCountryCode(),
            'region' => $this->getRegion(),
            'region_type' => $this->getRegionType(),
            'area' => $this->getArea(),
            'city' => $this->getCity(),
            'city_guid' => $this->getCityGuid(),
            'city_type' => $this->getCityType(),
            'street' => $this->getStreet(),
            'street_type' => $this->getStreetType(),
            'house' => $this->getHouse(),
            'block' => $this->getBlock(),
            'flat' => $this->getFlat(),
            'porch' => $this->getPorch(),
            'intercom' => $this->getIntercom(),
            'comment' => $this->getComment(),
            'geo_lat' => $this->getGeoLat(),
            'geo_lon' => $this->getGeoLon(),
        ];
    }
}
