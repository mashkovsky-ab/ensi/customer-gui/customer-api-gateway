<?php


namespace App\Http\ApiV1\Support\Queries;

/**
 * Trait QueryBuilderFilterTrait
 * @package App\Http\ApiV1\Support\Queries
 * @mixin QueryBuilder
 */
trait QueryBuilderFilterTrait
{
    protected function fillFilters($request)
    {
        $httpFilters = $this->httpRequest->get('filter');
        if ($httpFilters) {
            $request->setFilter((object)$httpFilters);
        }
    }
}
