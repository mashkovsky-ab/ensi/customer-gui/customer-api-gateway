<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Http\ApiV1\Modules\Catalog\Queries\Products\ProductsPimQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductsController
{
    public function get(int $productId, ProductsPimQuery $query): ProductsResource
    {
        return new ProductsResource($query->find($productId));
    }

    public function search(ProductsPimQuery $query): AnonymousResourceCollection
    {
        return ProductsResource::collectPage($query->get());
    }
}
