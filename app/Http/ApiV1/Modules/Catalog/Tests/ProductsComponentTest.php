<?php

use App\Domain\Catalog\Tests\Factories\Offers\OfferFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/products:search success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->withId(38)->makeResponseSearch([], 2),
    ]);
    $basePrice = 1440;
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->makeResponseSearch(['product_id' => 38, 'base_price' => $basePrice]),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/products:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'name', 'main_image', 'price']]])
        ->assertJsonPath('data.0.price', $basePrice);
});

test('GET /api/v1/catalog/products/{id} success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $productId = 514;
    $this->mockPimProductsApi()->allows([
        'getProduct' => ProductFactory::new()->withId($productId)->makeResponseOne(),
    ]);
    $basePrice = 12270;
    $this->mockOffersOffersApi()->allows([
        'searchOffers' => OfferFactory::new()->makeResponseSearch(['product_id' => $productId, 'base_price' => $basePrice]),
    ]);

    getJson("/api/v1/catalog/products/$productId")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'price', 'main_image']])
        ->assertJsonPath('data.price', $basePrice);
});
