<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Domain\Catalog\Data\Products\ProductData;
use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoriesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\BrandsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductDraft;

/**
 * @mixin ProductData
 * @mixin ProductDraft
 * @mixin Product
 */
abstract class BaseProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return array_merge([
            'id' => $this->getId(),

            'category_id' => $this->getCategoryId(),
            'brand_id' => $this->getBrandId(),

            'code' => $this->getCode(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'type' => $this->getType(),
            'vendor_code' => $this->getVendorCode(),
            'barcode' => $this->getBarcode(),

            'weight' => $this->getWeight(),
            'weight_gross' => $this->getWeightGross(),
            'length' => $this->getLength(),
            'height' => $this->getHeight(),
            'width' => $this->getWidth(),
            'is_adult' => $this->getIsAdult(),
            'price' => $this->getBasePrice(),

            'category' => new CategoriesResource($this->whenNotNull($this->getCategory())),
            'brand' => new BrandsResource($this->whenNotNull($this->getBrand())),
            'images' => ProductImagesResource::collection($this->whenNotNull($this->getImages())),
            'attributes' => ProductAttributeValuesResource::collection($this->whenNotNull($this->getAttributes())),
        ], $this->extraFields());
    }

    protected function getBasePrice(): ?int
    {
        return $this->resource instanceof ProductData
            ? $this->resource->getBasePrice()
            : null;
    }

    protected function extraFields(): array
    {
        return [];
    }
}
