<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Attributes;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Property;

/**
 * @mixin Property
 */
class ProductAttributesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'type' => $this->getType(),
            'name' => $this->getName(),
            'display_name' => $this->getDisplayName(),

            'is_system' => $this->getIsSystem(),
            'is_common' => $this->getIsCommon(),
            'is_active' => $this->getIsActive(),

            'is_multiple' => $this->getIsMultiple(),
            'is_filterable' => $this->getIsFilterable(),
            'is_public' => $this->getIsPublic(),
            'is_required' => $this->getIsRequired(),
            'has_directory' => $this->getHasDirectory(),
            'is_moderated' => $this->getIsModerated(),

            'hint_value' => $this->getHintValue(),
            'hint_value_name' => $this->getHintValueName(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'directory' => DirectoryValuesResource::collection($this->whenNotNull($this->getDirectory())),
        ];
    }
}
