<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Domain\Catalog\Data\Products\ProductData;
use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\PaginationTypeEnum;
use Ensi\OffersClient\Dto\RequestBodyPagination;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductResponse;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchProductsResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use stdClass;

class ProductsPimQuery extends PimQuery
{
    public function __construct(
        Request $request,
        private readonly ProductsApi $productsApi,
        private readonly OffersApi $offersApi
    ) {
        parent::__construct($request, SearchProductsRequest::class);
    }

    /**
     * @param SearchProductsRequest $request
     */
    protected function fillFilters($request)
    {
        parent::fillFilters($request);

        /** @var stdClass $filter */
        $filter = $request->getFilter();
        $filter->allow_publish = true;
        $request->setFilter((object)$filter);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id): ProductResponse
    {
        return $this->productsApi->getProduct($id, $this->getInclude());
    }

    protected function search($request): SearchProductsResponse
    {
        return $this->productsApi->searchProducts($request);
    }

    /**
     * @param ProductResponse $response
     * @return ProductData|null
     */
    protected function convertFindToItem($response): ?ProductData
    {
        return $this->mergeOffers([$response->getData()])->first();
    }

    /**
     * @param SearchProductsResponse $response
     * @return Collection<ProductData>
     */
    protected function convertGetToItems($response): Collection
    {
        return $this->mergeOffers($response->getData());
    }

    protected function mergeOffers(array $products): Collection
    {
        $products = new Collection($products);
        $offers = $this->loadOffers($products);

        return $products->map(fn (Product $product) => new ProductData($product, $offers->get($product->getId())));
    }

    /**
     * @param Collection<Product> $products
     * @return Collection<Offer>
     * @throws \Ensi\OffersClient\ApiException
     */
    private function loadOffers(Collection $products): Collection
    {
        //todo Пока фильтруем только по товарам, т.к. 1 товар = 1 офферу. В будущем добавить фильтр по seller_id
        $filter = [
            'product_id' => $products->pluck('id'),
        ];

        $request = new SearchOffersRequest();
        $request->setFilter((object)$filter);
        $request->setPagination(
            (new RequestBodyPagination())
                ->setType(PaginationTypeEnum::CURSOR)
                ->setLimit($products->count())
        );

        return collect($this->offersApi->searchOffers($request)->getData())
            ->keyBy('product_id');
    }
}
