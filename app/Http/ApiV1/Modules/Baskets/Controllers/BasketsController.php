<?php

namespace App\Http\ApiV1\Modules\Baskets\Controllers;

use App\Domain\Orders\Actions\Baskets\DeleteBasketAction;
use App\Domain\Orders\Actions\Baskets\SetBasketItemsAction;
use App\Http\ApiV1\Modules\Baskets\Queries\BasketsQuery;
use App\Http\ApiV1\Modules\Baskets\Requests\SetBasketItemRequest;
use App\Http\ApiV1\Modules\Baskets\Resources\BasketsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class BasketsController
{
    public function setItem(SetBasketItemRequest $request, SetBasketItemsAction $action)
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function current(BasketsQuery $query): BasketsResource
    {
        return new BasketsResource($query->current());
    }

    public function delete(DeleteBasketAction $action)
    {
        $action->execute();

        return new EmptyResource();
    }
}
