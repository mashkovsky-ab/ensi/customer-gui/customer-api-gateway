<?php

use App\Domain\Catalog\Tests\Factories\Offers\OfferFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Domain\Orders\Tests\Baskets\Factories\BasketFactory;
use App\Domain\Orders\Tests\Baskets\Factories\BasketItemFactory;
use App\Http\ApiV1\Modules\Baskets\Tests\Factories\BasketItemsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\Factories\PromiseFactory;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/baskets:set-item 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockBasketsBasketApi()->shouldReceive('setBasketCustomerItem');

    $request = BasketItemsRequestFactory::new()->make();
    postJson('/api/v1/baskets:set-item', $request)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/baskets:current 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $offerId = 1;
    $productId = 2;
    $item = BasketItemFactory::new()->make(['offer_id' => $offerId, 'product_id' => $productId]);

    $this->mockBasketsBasketApi()->allows([
        'searchBasketCustomer' => BasketFactory::new()->withItems($item)->makeResponseSearch(),
    ]);

    $this->mockPimProductsApi()->allows([
        'searchProductsAsync' => PromiseFactory::make(ProductFactory::new()->makeResponseSearch(['id' => $productId])),
    ]);

    $this->mockOffersOffersApi()->allows([
        'searchOffersAsync' => PromiseFactory::make(OfferFactory::new()->makeResponseSearch(['id' => $offerId])),
    ]);

    postJson('/api/v1/baskets:current')
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $offerId);
});

test('DELETE /api/v1/baskets 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockBasketsBasketApi()->shouldReceive('deleteBasketCustomer');

    deleteJson('/api/v1/baskets')
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});
