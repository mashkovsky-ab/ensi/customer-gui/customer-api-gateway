<?php

namespace App\Http\ApiV1\Modules\Baskets\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class BasketItemsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'items' => [
                [
                    'offer_id' => $this->faker->numberBetween(1),
                    'qty' => $this->faker->randomFloat(2, 1, 100),
                ],
            ],
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
