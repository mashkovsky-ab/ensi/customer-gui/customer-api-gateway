<?php


namespace App\Http\ApiV1\Modules\Baskets\Resources;

use App\Domain\Orders\Data\Baskets\BasketData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class BasketsResource
 * @package App\Http\ApiV1\Modules\Baskets\Resources
 * @mixin BasketData
 */
class BasketsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return BasketItemsResource::collection($this->getBasketItems());
    }
}
