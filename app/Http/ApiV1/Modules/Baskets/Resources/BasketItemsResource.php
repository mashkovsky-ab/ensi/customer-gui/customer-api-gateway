<?php


namespace App\Http\ApiV1\Modules\Baskets\Resources;

use App\Domain\Orders\Data\Baskets\BasketItemData;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductImagesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class BasketItemsResource
 * @package App\Http\ApiV1\Modules\Baskets\Resources
 * @mixin BasketItemData
 */
class BasketItemsResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->getOfferId(),
            'name'          => $this->getName(),
            'code'          => $this->getCode(),
            'barcode'       => $this->getBarcode(),
            'vendor_code'   => $this->getVendorCode(),
            'type'          => $this->getType(),
            'price_per_one' => $this->getBasePrice(),
            'price'         => (int)($this->getBasePrice() * $this->getQty()),
            'qty'           => $this->getQty(),
            'is_adult'      => $this->getIsAdult(),
            'main_image'    => $this->getMainImage(),
            'images'        => ProductImagesResource::collection($this->whenNotNull($this->getImages())),
        ];
    }
}
