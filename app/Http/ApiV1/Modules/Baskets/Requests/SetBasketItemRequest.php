<?php

namespace App\Http\ApiV1\Modules\Baskets\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SetBasketItemRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'items' => ['required', 'array'],
            'items.*.offer_id' => ['required', 'integer'],
            'items.*.qty' => ['required', 'numeric'],
        ];
    }
}
