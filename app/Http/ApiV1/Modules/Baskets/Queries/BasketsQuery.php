<?php

namespace App\Http\ApiV1\Modules\Baskets\Queries;

use App\Domain\Orders\Data\Baskets\BasketData;
use App\Domain\Orders\Data\Baskets\BasketItemData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use Ensi\BasketsClient\Api\CustomerBasketsApi;
use Ensi\BasketsClient\Dto\Basket;
use Ensi\BasketsClient\Dto\BasketItem;
use Ensi\BasketsClient\Dto\SearchBasketCustomerRequest;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\RequestBodyPagination as OffersRequestPagination;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\RequestBodyPagination as PimRequestPagination;
use Ensi\PimClient\Dto\SearchProductsFilter;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchProductsResponse;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use LogicException;

class BasketsQuery extends QueryBuilder
{
    /**
     * @var bool
     */
    protected bool $loadImages = false;

    /**
     * @var Offer[]
     */
    protected Collection $offers;

    /**
     * @var Product[]
     */
    protected Collection $products;

    public function __construct(
        Request $request,
        private readonly CustomerBasketsApi $basketsApi,
        private readonly ProductsApi $productsApi,
        private readonly OffersApi $offersApi
    ) {
        parent::__construct($request);
    }

    public function current()
    {
        $request = new SearchBasketCustomerRequest([
            'customer_id' => user()?->getCustomerId(),
            'include' => ['items'],
        ]);

        $this->fillInclude($request);

        $response = $this->basketsApi->searchBasketCustomer($request);

        return $this->convertFindToItem($response);
    }

    protected function convertFindToItem($response)
    {
        return current($this->convertArray([$response->getData()]));
    }

    protected function convertArray(array $baskets): array
    {
        /** @var Collection|Basket[] $baskets */
        $baskets = collect($baskets);

        $offerIds = collect();
        $productIds = collect();
        foreach ($baskets as $basket) {
            /** @var BasketItem $item */
            foreach ($basket->getItems() ?? [] as $item) {
                $offerIds->push($item->getOfferId());
                $productIds->push($item->getProductId());
            }
        }

        Utils::all(array_filter([
            $this->loadOffers($this->prepareFilterValues($offerIds)),
            $this->loadProducts($this->prepareFilterValues($productIds)),
        ]))->wait();

        $basketsData = [];
        foreach ($baskets as $basket) {
            $basketData = new BasketData($basket);
            foreach ($basket->getItems() ?? [] as $item) {
                $offerId = $item->getOfferId();
                $productId = $item->getProductId();
                if (!$this->offers->has($offerId) || !$this->products->has($productId)) {
                    throw new LogicException('В корзине присутствует несуществующий товар');
                }

                $basketItemData = new BasketItemData(
                    $this->products[$productId],
                    $this->offers[$offerId],
                    $item
                );
                $basketData->addBasketItem($basketItemData);
            }

            $basketsData[] = $basketData;
        }

        return $basketsData;
    }

    protected function prepareFilterValues(Collection $values): array
    {
        return $values->unique()->values()->filter()->all();
    }

    protected function loadOffers(array $ids): PromiseInterface
    {
        $request = new SearchOffersRequest();
        $request->setFilter((object)['id' => $ids]);
        $request->setPagination((new OffersRequestPagination())->setLimit(count($ids)));

        return $this->offersApi->searchOffersAsync($request)
            ->then(function (SearchOffersResponse $response) {
                $this->offers = collect($response->getData())->keyBy(function (Offer $offer) {
                    return $offer->getId();
                });
            });
    }

    protected function loadProducts(array $ids): PromiseInterface
    {
        $filter = new SearchProductsFilter();
        $filter->setId($ids);
        $request = new SearchProductsRequest();
        $request->setFilter($filter);
        $request->setPagination((new PimRequestPagination())->setLimit(count($ids)));

        if ($this->loadImages) {
            $request->setInclude(['images']);
        }

        return $this->productsApi->searchProductsAsync($request)
            ->then(function (SearchProductsResponse $response) {
                $this->products = collect($response->getData())->keyBy(function (Product $product) {
                    return $product->getId();
                });
            });
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "images":
                    $this->loadImages = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }
}
