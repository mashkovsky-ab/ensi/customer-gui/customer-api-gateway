<?php

namespace App\Http\ApiV1\Modules\Auth\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CustomerAuthClient\Dto\CreateTokenResponse;

/**
 * Class TokensResource
 * @package App\Http\ApiV1\Modules\Auth\Resources
 * @mixin CreateTokenResponse
 */
class TokensResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request): array
    {
        return [
            'access_token' => $this->getAccessToken(),
            'refresh_token' => $this->getRefreshToken(),
            'expires_in' => $this->getExpiresIn(),
        ];
    }
}
