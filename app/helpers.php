<?php

use App\Domain\Auth\Models\User;

if (!function_exists('in_production')) {

    /**
     * Находится ли приложение в прод режиме
     * @return bool
     */
    function in_production(): bool
    {
        return app()->environment('production');
    }
}

if (!function_exists('user')) {
    /**
     * Возвращает текущего пользователя
     * @return User;|null
     */
    function user(): ?User
    {
        return auth()->user();
    }
}
