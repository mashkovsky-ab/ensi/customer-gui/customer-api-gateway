<?php

return [
    'catalog' => [
        'offers' => [
            'base_uri' => env('CATALOG_OFFERS_SERVICE_HOST') . "/api/v1",
        ],
        'pim' => [
            'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'customers' => [
        'customer-auth' => [
            'base_uri' => env('CUSTOMERS_CUSTOMER_AUTH_SERVICE_HOST') . "/api/v1",
            'client' => [
                'id' => env('CUSTOMERS_CUSTOMER_AUTH_SERVICE_CLIENT_ID', ''),
                'secret' => env('CUSTOMERS_CUSTOMER_AUTH_SERVICE_CLIENT_SECRET', ''),
            ],
        ],
    ],
    'orders' => [
        'baskets' => [
            'base_uri' => env('ORDERS_BASKETS_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
